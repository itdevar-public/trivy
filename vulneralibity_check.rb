# frozen_string_literal: true

require 'json'
require 'open-uri'
require 'net/http'
require 'set' # Para utilizar la clase Set
require 'tracer'

def log(message)
  puts "LOG: #{message}"
end

def determine_tag
  log("Determinando la etiqueta basada en CI_COMMIT_REF_NAME...")
  case ENV['CI_COMMIT_REF_NAME']
  when 'dev'
    'dev'
  when 'test'
    'test'
  when 'master'
    'master'
  else
    log("La rama #{ENV['CI_COMMIT_REF_NAME']} no corresponde a un entorno válido.")
    exit(1)
  end
end

def extract_vulnerability_ids(critical_blocks)
  critical_blocks.scan(/CVE-\d+-\d+/).flatten
end

def get_existing_cve
  log("Recuperando CVEs existentes...")
  uri = URI("https://gitlab.com/itdevar/api/v4/projects/#{ENV['CI_PROJECT_ID']}/issues?search=#{ENV['CI_PROJECT_NAME']}&label_name=Vulnerabilidad&state=opened&scope=all")
  req = Net::HTTP::Get.new(uri)
  req['PRIVATE-TOKEN'] = ENV['CHANGELOG_TOKEN']

  begin
    res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) { |http| http.request(req) }
    log("Respuesta de API para CVEs existentes: #{res.body}") # Nuevo log
    extract_vulnerability_ids(res.body)
  rescue StandardError => e
    log("Error al obtener CVE existente: #{e.message}, Trace: #{e.backtrace.join("\n")}") # Nuevo log
    []
  end
end

def format_vulnerabilities(vulnerabilities)
  output = []
  output << "<table border='1'>"
  output << "<tr><th>LIBRARY</th><th>VULNERABILITY ID</th><th>SEVERITY</th><th>INSTALLED VERSION</th><th>FIXED VERSION</th></tr>"

  vulnerabilities.each do |vuln|
    output << "<tr><td>#{vuln[:library]}</td><td>#{vuln[:vuln_id]}</td><td>#{vuln[:severity]}</td><td>#{vuln[:installed_version]}</td><td>#{vuln[:fixed_version]}</td></tr>"
  end

  output << "</table>"
  output.join("\n")
end

# DEBUG
# Tracer.on

# Inicio del script
tag = determine_tag
log("INFO: Comenzando a ejecutarse en #{tag}")

# Verificación de token
log("Verificando token: #{ENV['CHANGELOG_TOKEN'].nil? ? 'No definido' : 'Definido'}") # Nuevo log

# Ejecución de Trivy y manejo de posibles errores
begin
  log("Iniciando el análisis de Trivy para la imagen #{ENV['CI_REGISTRY_IMAGE']}:#{tag}") # Nuevo log
  result_trivy = `trivy image --no-progress -f json --severity HIGH,LOW,MEDIUM,CRITICAL #{ENV['CI_REGISTRY_IMAGE']}:#{tag}`
  json_data = JSON.parse(result_trivy)

  detected_cves = json_data['Results'].flat_map do |res|
    res['Vulnerabilities'] ? res['Vulnerabilities'].map { |vuln| vuln['VulnerabilityID'] } : []
  end.uniq
  
  #detected_cves = json_data['Results'].flat_map { |res| res['Vulnerabilities'].map { |vuln| vuln['VulnerabilityID'] } }.uniq
  log("CVEs detectados: #{detected_cves.join(", ")}")
  
  # Guarda los resultados del escaneo de Trivy en scanning-report.json
  File.open('scanning-report.json', 'w') do |f|
    f.write(JSON.pretty_generate(json_data))
  end

rescue StandardError => e
  log("Error al ejecutar Trivy: #{e.message}, Trace: #{e.backtrace.join("\n")}") # Nuevo log
  exit(1)
end


existing_cves = get_existing_cve # Llamada única para obtener los CVEs existentes
log("CVEs existentes: #{existing_cves.join(", ")}")

# ... (Resto del código para comparación de CVEs y creación de "issues") ...

if json_data['Results'].any? { |res| res['Vulnerabilities'] && !res['Vulnerabilities'].empty? }

  existing_cves = Set.new(existing_cves)
  new_cves = []
  
  # Counter issue CVE
  existing_cve_count = 0

  json_data['Results'].each do |res|
    if res['Vulnerabilities']
        res['Vulnerabilities'].each do |vulnerability|
          vulnerability_id = vulnerability['VulnerabilityID']
          if existing_cves.include?(vulnerability_id)
            log("#{vulnerability_id} ya existe, no agregar.")
            # Counter issue CVE
            existing_cve_count += 1
          else
            log("#{vulnerability_id} es nuevo, agregando.")
            new_cves << vulnerability_id
          end
        end
    end
  end
  
  new_cves.uniq!

if existing_cve_count > 0
  log("Se encontraron #{existing_cve_count} vulnerabilidades que ya tienen issues asociados. Terminando el pipeline con advertencia.")
  exit(1)
elsif new_cves.any?
    # STOP pipeline
    log("Se encontraron vulnerabilidades. Deteniendo el pipeline.")
    vulnerabilities = new_cves.map do |cve_id|
      vuln_obj = json_data['Results'].flat_map { |res| res['Vulnerabilities'] }.compact.find { |vuln| vuln['VulnerabilityID'] == cve_id }
      {
        library: vuln_obj['PkgName'],
        vuln_id: cve_id,
        severity: vuln_obj['Status'],
        installed_version: vuln_obj['InstalledVersion'],
        fixed_version: vuln_obj['FixedVersion']
      }
    end

    formatted_vulnerabilities = format_vulnerabilities(vulnerabilities)
    description = "Vulnerabilities:\n#{formatted_vulnerabilities}"

    uri = URI("https://gitlab.com/itdevar/api/v4/projects/#{ENV['CI_PROJECT_ID']}/issues")
    req = Net::HTTP::Post.new(uri)
    req['PRIVATE-TOKEN'] = ENV['CHANGELOG_TOKEN']
    req.set_form_data('title' => "Critical vulnerability in #{ENV['CI_PROJECT_NAME']}", 'description' => description)

    begin
      res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) { |http| http.request(req) }
      if res.code == '201'
        log('Nuevo problema creado exitosamente.')
      else
        log("Error al crear el problema. Código de respuesta: #{res.code}")
        log("Cuerpo de la respuesta: #{res.body}")
      end
    rescue StandardError => e
      log("Error al comunicarse con GitLab para la URI #{uri}: #{e.message}, Trace: #{e.backtrace.join("\n")}")
    end
    # STOP pipeline
    exit(1)  # Detiene el script y, por ende, el pipeline
  end
end